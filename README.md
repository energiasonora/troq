# TROQ

## Getting started
A self custodial chat-wallet for bartering

<!-- # LOG
- NEW! v2.0Beta. With polybase! -->

##  Contracts
- slave (scroll sepolia testnet) 0xF927099D89378611a0cDF690580977Ca7af425BA
- master (scroll sepolia testnet) 0xc7a280CbF424d76cf42FcE9d739032E72658490D

## Installation

```
cd TROQ
nvm install 18
yarn install 
yarn parcel src/index.html --port 2121 --https

```

# To compile demo (just for main dev)
```
yarn parcel src/index.html --dist-dir public  --public-url ./
```

# deploy to firebase
firebase deploy

## Links
- [ ] [UPDATED Demo](https://energiasonora.gitlab.io/troq)
- [ ] [Old demo](https://solo-81fbf.web.app/)

